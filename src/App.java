//UI
import javax.swing.*;
//Eventos
import java.awt.event.*;

public class App extends JFrame{
    /**************
     * Atributos
     **************/
    private JTextField campo_1;
    private JTextField campo_2;
    private JButton btnSumar;
    private JLabel lblResultado;

    /**************
     * Constructor
     **************/
    public App(){
        /*****************
         * Configuración
         ***************/
        this.setTitle("Calculadora");
        //Coordenadas donde se debe aparecer la ventana en x, 'y'. y el ancho y alto de la ventana
        this.setBounds(500, 400, 200, 250);
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        /*****************
         * Inicializar
         * los atributos
         ****************/
        this.campo_1 = new JTextField();
        this.campo_1.setBounds(50, 20, 100, 30);
        this.add(this.campo_1);

        this.campo_2 = new JTextField();
        this.campo_2.setBounds(50, 60, 100, 30);
        this.add(this.campo_2);

        this.btnSumar = new JButton("Sumar");
        this.btnSumar.setBounds(50, 100, 100, 40);
        this.add(this.btnSumar);

        this.lblResultado = new JLabel("Resultado: ");
        this.lblResultado.setBounds(60, 150, 100, 30);
        this.add(lblResultado);

        //Poner el botón a la escucha de un evento
        this.btnSumar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                //Obtener la entrada del usuario y convertirla a un entero
                int num_1 = Integer.parseInt(campo_1.getText());
                int num_2 = Integer.parseInt(campo_2.getText());
                int suma = num_1 + num_2;                
                lblResultado.setText("Resultado: "+suma);
                //Limpiar campos de texto
                campo_1.setText("");
                campo_2.setText("");
            }
        } );


        this.setVisible(true);
    }

    
    public static void main(String[] args) throws Exception {
        new App();
    }
}

